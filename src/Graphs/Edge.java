package Graphs;

public class Edge implements Comparable<Edge>{
    //elke edge (lijn dus) heeft een startpunt, een eindpunt en een waarde (weight)
    Node source;
    Node destination;
    double weight;

    Edge(Node s, Node d, double w){
        this.source = s;
        this.destination = d;
        this.weight = w;
    }

    @Override
    public String toString() {

        return "Edge{" +
                "source=" + source.name +
                ", destination=" + destination.name +
                ", weigth=" + weight +
                '}';
    }

    @Override
    public int compareTo(Edge o) {
        if (this.weight > o.weight) {
            return 1;
        }
        else return -1;
    }
}

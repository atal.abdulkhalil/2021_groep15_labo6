package Graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class AbLeesBestand {
    public List<Player> leesSpelers(String bestand){
        List<Player> players = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(bestand));

            String lijn;
            String[] info;
            String vorigeNaam = "";
            while (sc.hasNextLine()) {
                lijn = sc.nextLine().trim();
                info = lijn.split(";");

                if (vorigeNaam.equals(info[0])){
                    Player player = players.get(players.size() - 1);
                    player.addClub(info[2], info[1]);
                    vorigeNaam = info[0];
                } else{
                    String[] finalInfo = info;
                    players.add(new Player(info[0], new HashMap<String, String>() {{
                        put(finalInfo[2], finalInfo[1]);
                    }}));
                    vorigeNaam = info[0];
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }

        return players;

    }

}

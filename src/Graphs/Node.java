package Graphs;

import java.util.LinkedList;

public class Node {
    //elk punt heeft een naam, boolean die kijkt of hij al bezocht is en een 'edge' (lees: lijn tussen de hoekpunten)
    Player name;
    private boolean visited;
    LinkedList<Edge> edges;

    Node(Player n){
        this.name = n;
        visited = false;
        edges = new LinkedList<>();
    }

    boolean isVisited(){
        return visited;
    }

    void visit(){
        visited = true;
    }

    void unvisit(){
        visited = false;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name=" + name +
                '}';
    }
}

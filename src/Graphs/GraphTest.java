package Graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class GraphTest {
    public static void main(String[] args) {
        List<Player> players = leesSpelers("voetballers.txt");
        Set<Node> nodes = new HashSet<>();

        for (Player p: players) {
             nodes.add(new Node(p));
        }

        Graph graph = new Graph(false);

       for (Node n: nodes){
           for (Node x: nodes){
               if(n == x){
                   continue;
               }
               for(Map.Entry<String, String> entry : n.name.getClubPerSeizoen().entrySet()){

               }
           }
       }
//        graph.addEdge(nul, twee, 2);
//        graph.addEdge(twee, een, 7);
//        graph.addEdge(twee, drie, 1);
//        graph.addEdge(twee, vier, 9);
//        graph.addEdge(een, vier, 8);
//        graph.addEdge(een, drie, 3);
//        graph.addEdge(drie, vier, 5);
//        graph.addEdge(drie, vijf, 2);
//        graph.addEdge(vier, vijf, 1);
//        graph.addEdge(vijf, zes, 8);
//        graph.addEdge(vier, zes, 6);
//
//        graph.Dijkstra(nul, zes);
    }

    public static List<Player> leesSpelers(String bestand){
        List<Player> players = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(bestand));

            String lijn;
            String[] info;
            String vorigeNaam = "";
            while (sc.hasNextLine()) {
                lijn = sc.nextLine().trim();
                info = lijn.split(";");

                if (vorigeNaam.equals(info[0])){
                    Player player = players.get(players.size() - 1);
                    player.addClub(info[2], info[1]);
                    vorigeNaam = info[0];
                } else{
                    String[] finalInfo = info;

                    players.add(new Player(info[0], new HashMap<String, String>() {{
                        put(finalInfo[2], finalInfo[1]);
                    }}));
                    vorigeNaam = info[0];
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }

        return players;

    }

}

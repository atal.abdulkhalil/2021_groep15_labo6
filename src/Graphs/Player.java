package Graphs;

import java.util.HashMap;
import java.util.Map;

public class Player implements Comparable<Player>{
    private String naam;
    private Map<String, String> clubPerSeizoen;

    public Player() {
    }

    public Player(String naam, Map<String, String> clubPerSeizoen) {
        this.naam = naam;
        this.clubPerSeizoen = clubPerSeizoen;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Map getClubPerSeizoen() {
        return clubPerSeizoen;
    }

    public void setClubPerSeizoen(HashMap clubPerSeizoen) {
        this.clubPerSeizoen = clubPerSeizoen;
    }

    public void addClub(String key, String value){
        clubPerSeizoen.put(key, value);
    }

    @Override
    public String toString() {
        return "spelersnaam = " + naam ;
    }

    @Override
    public int compareTo(Player o) {
        return 0;
    }
}
